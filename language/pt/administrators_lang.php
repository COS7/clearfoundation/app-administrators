<?php

$lang['administrators_app_description'] = 'Com o aplicativo Administradores, você pode conceder acesso a aplicativos específicos para grupos de usuários no sistema.';
$lang['administrators_app_name'] = 'Administradores';
