<?php

$lang['administrators_app_description'] = '管理者のアプリを使用すると、システム上のユーザーのグループに特定のアプリへのアクセスを許可することができます。';
$lang['administrators_app_name'] = '管理者';
